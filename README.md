# Atlassian Jira in a Docker container


A containerized installation of Atlassian Jira for use in the Affinityps intranet.

## Build an affinityps/jira Image

This repository is linked to the thomasbjackson/jira respository in Docker hub.

This image will therefore get auto-built whenever this repo is merged to one of the branches specified in the build details tab in the docker repo.

Alternatively, a manual build can be performed by download the contents of this repo to any linux host with a docker daemon, then use

```
sudo docker build -t affinityps/jira:6.3.15 .
```


## Provision a Host

Container runs on ports 8090 and 8443 and must be mapped to host port when run.


```
sudo docker run --name jira -v /var/local/atlassian/jira:/var/local/atlassian/jira -p 80:8080 -p 443:8443 -t affinityps/jira:6.3.15
```

## Volumes

Volume mount points are provided for installation and home directories.

Key use case for home volume is backups.

## SSL Certs

Existing SSL certs for Jira and Confluence are in the config/certs directory, as is the private key used to generate the Jira cert CSR.

When the Jira cert expires, use the private key in configs/certs to generate the CSR.

When the new Jira cert arrives, overwrite the existing cert in configs/certs and re-build the image.

## Alternate Environments

The image is 99% compatible with any host environment. There is however one file that "hardcodes" the image to the jira.affinityps.com FQDN. See configs/urlrewrite.xml. This file rewrites browser-based requests for "jira" and "jira.affinityps.com" to https://jira.affinityps.com. 


To provision a host with this image with a cname other than "jira" (e.g. "jiratest") the urlrewrite.xml file must be hand edited as follows:

	1. Provision the host with the image the usual way (see above)
	2. Attach to the runing container using: `sudo docker exec -it jira /bin/bash`
	3. Use vi to edit urlrewrite.xml: `vi $CONFLUENCE_INSTALL/confluence/WEB-INF/urlwrite.xml`
	4. Detach from the running container by typing `exit`
	5. Restart the container: `sudo docker restart jira`

